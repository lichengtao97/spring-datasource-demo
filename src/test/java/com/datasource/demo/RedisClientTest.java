//package com.xinhuo.demo;
//
//import com.xinhuo.demo.util.RedisClient;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.test.context.junit4.SpringRunner;
//
///**
// * @Auther: LI
// * @Date: 2021/10/10 11:11:40
// * @Description:
// */
//@RunWith(SpringRunner.class)
//@SpringBootTest
//public class RedisClientTest {
//    @Autowired
//    RedisClient redisClient;
//
//    @Test
//    public void testRedis() {
//        try{
//            redisClient.set("testKey","redis测试值");
//            String testValue = redisClient.get("testKey");
//            System.out.println("从redis获取的key值："+testValue);
//        }catch (Exception e){
//            e.printStackTrace();
//        }
//    }
//}
