package com.datasource.demo.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.datasource.demo.entity.Teacher;

/**
 * @Auther: LI
 * @Date: 2021/10/10 15:15:55
 * @Description:
 */
public interface TeacherService extends IService<Teacher>{

}
