package com.datasource.demo.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.datasource.demo.entity.Teacher;
import com.datasource.demo.mapper.slave.TeacherMapper;
import com.datasource.demo.service.TeacherService;
import org.springframework.stereotype.Service;

/**
 * @Auther: LI
 * @Date: 2021/10/10 16:16:06
 * @Description:
 */
@Service
public class TeacherServiceImpl extends ServiceImpl<TeacherMapper,Teacher> implements TeacherService {
}
