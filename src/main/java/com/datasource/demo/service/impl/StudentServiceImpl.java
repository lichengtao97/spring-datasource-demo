package com.datasource.demo.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.datasource.demo.entity.Student;
import com.datasource.demo.mapper.master.StudentMapper;
import com.datasource.demo.service.StudentService;
import org.springframework.stereotype.Service;

/**
 * @Auther: LI
 * @Date: 2021/10/10 16:16:06
 * @Description:
 */
@Service
class StudentServiceImpl  extends ServiceImpl<StudentMapper,Student> implements StudentService {
}