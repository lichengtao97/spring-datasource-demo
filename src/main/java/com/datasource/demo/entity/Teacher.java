package com.datasource.demo.entity;

import com.baomidou.mybatisplus.annotation.TableName;

/**
 * @Auther: LI
 * @Date: 2021/10/10 15:15:43
 * @Description:
 */
@TableName("teacher")
public class Teacher {
    private int id;
    private String name;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
