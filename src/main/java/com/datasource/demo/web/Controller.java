package com.datasource.demo.web;

import com.datasource.demo.entity.Student;
import com.datasource.demo.entity.Teacher;
import com.datasource.demo.mapper.master.StudentMapper;
import com.datasource.demo.mapper.slave.TeacherMapper;
import com.datasource.demo.service.StudentService;
import com.datasource.demo.service.TeacherService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Auther: LI
 * @Date: 2019/5/17 10:10:05
 * @Description: spring aop demo
 */
@RestController
@RequestMapping("/")
public class Controller {

    Logger log = LoggerFactory.getLogger(getClass());

    @Autowired
    private StudentService studentService;

    @Autowired
    private TeacherService teacherService;

    @Autowired
    private StudentMapper studentMapper;

    @Autowired
    private TeacherMapper teacherMapper;


    /**
     * 查询学生信息
     * @return
     */
    @GetMapping("/findStudent")
    public Student findStudent(int sid) {
        log.info("进入findStudent start");
        if(sid == 0){
            throw new RuntimeException("参数sid为空");
        }
//        Student stu = studentService.getById(sid);
        Student stu =studentMapper.selectById(sid);
        return stu;
    }

    /**
     * 查询老师信息
     * @return
     */
    @GetMapping("/findTeacher")
    public Teacher findTeacher(int sid) {
        log.info("findTeacher start");
        if(sid == 0){
            throw new RuntimeException("参数sid为空");
        }
//        Teacher tea = teacherService.getById(sid);
        Teacher tea = teacherMapper.selectById(sid);
        return tea;
    }
}
