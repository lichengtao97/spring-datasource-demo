package com.datasource.demo.mapper.master;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.datasource.demo.entity.Student;

/**
 * @Auther: LI
 * @Date: 2021/10/10 15:15:46
 * @Description:
 */
public interface StudentMapper extends BaseMapper<Student> {
}
