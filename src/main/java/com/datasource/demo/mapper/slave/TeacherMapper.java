package com.datasource.demo.mapper.slave;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.datasource.demo.entity.Teacher;

/**
 * @Auther: LI
 * @Date: 2021/10/10 15:15:47
 * @Description:
 */
public interface TeacherMapper extends BaseMapper<Teacher> {
}
